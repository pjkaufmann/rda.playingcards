// Playing Cards Pt. 2
// Phoenix Kaufmann

#include <iostream>
#include <conio.h>
using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Hearts, Spades, Diamonds, Clubs };

struct Card
{
	Rank Rank;
	Suit Suit;
};

// Function preloading
void PrintCard(Card);
Card HighCard(Card, Card);

int main()
{
	// Declare & initialize 2 cards
	Card card1;
	Card card2;
	
	card1.Rank = Ace;
	card1.Suit = Spades;

	card2.Rank = Jack;
	card2.Suit = Clubs;

	// Print the cards and stuff
	cout << "\nThe first card is: ";
	PrintCard(card1);

	cout << "\nThe second card is: ";
	PrintCard(card2);

	cout << "\n-------------------------------------";
	cout << "\n\nThe high card is: ";
	PrintCard(HighCard(card1, card2));

	(void)_getch();
	return 0;
}

// Print the card
void PrintCard(Card card)
{
	// Declare some string variables
	string strSuit;
	string strRank;

	// Convert enum to string...
	switch (card.Suit)
	{
		case 0: strSuit = "Hearts"; break;
		case 1: strSuit = "Spades"; break;
		case 2: strSuit = "Diamonds"; break;
		case 3: strSuit = "Clubs"; break;
	}

	// Convert enum to string...
	switch (card.Rank)
	{
	case 2: strRank = "Two"; break;
	case 3: strRank = "Three"; break;
	case 4: strRank = "Four"; break;
	case 5: strRank = "Five"; break;
	case 6: strRank = "Six"; break;
	case 7: strRank = "Seven"; break;
	case 8: strRank = "Eight"; break;
	case 9: strRank = "Nine"; break;
	case 10: strRank = "Ten"; break;
	case 11: strRank = "Jack"; break;
	case 12: strRank = "Queen"; break;
	case 13: strRank = "King"; break;
	case 14: strRank = "Ace"; break;
	}

	// Print the card!
	cout << "The " << strRank << " of " << strSuit;
}

// Determine the higher of 2 cards
Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) return card1;	
	else return card2;
}